require 'rubygems'
require 'bundler'

Bundler.require

require './notfish'
run Sinatra::Application
