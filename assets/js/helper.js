// Helper functions {{

var topic_eventsource_connections = {};
var current_location = location.pathname.split('/');
var form_button_state = {'topic': true, 'category': true};
var form_sign_state = true;

var formHelper = {
  equal: function(val1, val2){
    return (val1 == val2);
  },

  minLength: function(val, length){
    return val.length >= length;
  },

  maxLength: function(val, length){
    return val.length <= length;
  }
};

var spin_opts = {
  lines: 6,
  length: 0,
  width: 5,
  radius: 5,
  corners: 1,
  rotate: 0,
  color: '#AAA',
  speed: 1,
  trail: 60,
  shadow: false,
  hwaccel: false,
  className: 'spinner',
  zIndex: 2e9,
  top: 'auto',
  left: 'auto'
}

function formPostHelpers(selector, callback){
  var current_form = selector;
  var form_data = $(current_form).serialize();

  $(current_form).children('button, input, textarea').attr('disabled', 'disabled');

  emile('loading-indicator', 'opacity: 1', {duration: 300});

  if ($.active == 0) {
    $.ajax({
      type: 'POST',
      url: $(current_form).attr('action'),
      data: form_data,
      complete: function(xhr, status){
        $(current_form).children('button, input, textarea').removeAttr('disabled');
        $(current_form).children('button, input, textarea').val('');

        emile('loading-indicator', 'opacity: 0', {duration: 300});
      },
      success: function(data){
        callback(JSON.parse(data));
      },
      error: function(xhr, errorType, error){
      }
    });
  }
}

function topicEventSource(category_id, topic_id){
  var source = new EventSource('/categories/' + category_id + '/topics/' + topic_id + '/events');

  topic_eventsource_connections[topic_id] = source;

  source.onopen = function(){
  };

  source.onmessage = function(event){
    var post_show_template = doT.template($('#post-show-template').html());
    var post_data = JSON.parse(event.data);

    if ($('#post-' + post_data.post._id).html() == null) {
      $('#second-container').append(post_show_template(post_data));

      var second_container_height = $('#second-container').css('height');
      emile('main-content', 'height: ' + second_container_height, {duration:500});
    }
  };
}

//}}
