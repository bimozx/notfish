Zepto(function($){

  // Events section {{

  $(document).on('form:send', function(e){
    emile('loading-indicator', 'opacity: 1;', {duration: 100});
  });

  $(document).on('form:complete', function(e){
    emile('loading-indicator', 'opacity: 0;', {duration: 100});
  });

  $(document).on('form:warning', function(e){
    $('input, textarea').off('focus');
    $('input, textarea').on('focus', function(e){
      var field_id = $(this).attr('id');

      $('#' + field_id + '_unhappy').remove();
    });
  });

  $(document).on('show:form', function(e, collection){
    $(document.body).trigger('form:warning');

    var validated_fields = {};
    var input_field = '#' + collection + '-form-title-input';
    var textarea_field = '#' + collection + '-form-body-textarea';

    validated_fields[input_field] = {
      required: true,
      message: "Sorry, you need to write the title"
    };

    validated_fields[textarea_field] = {
      required: true,
      message: "Sorry, a " + collection +" needs a description"
    };

    $('#' + collection + '-form').off('submit');
    $('#' + collection + '-form').isHappy({
      fields: validated_fields
    });

    if (form_button_state[collection]) {
      emile('slide-' + collection + '-button-container', 'margin-top: -24px;', {duration: 600});

      emile('new-' + collection + '-body', 'height: 176px;', {
              duration: 300,
              after: function(){
                form_button_state[collection] = false;
                $('#new-' + collection + '-form input').focus();
            }
      });

    } else {
      emile('slide-' + collection + '-button-container', 'margin-top: 0px;', {duration: 600});

      emile('new-' + collection + '-body', 'height: 28px;', {
              duration: 300,
              after: function(){
                form_button_state[collection] = true;
                $('#' + collection + '-form').children(['input', 'textarea']).val('');
                $('#' + collection + '-form').children(['input', 'textarea']).removeClass('unhappy');
                $('#' + collection + '-form').children(['.unhappyMessage']).remove();
            }
      });
    }
  });

  $(document).on('create:collection', function(e, collection, data){
    var collection_new_template = doT.template($('#' + collection + '-new-template').html());
    $('#new-' + collection + '-form').after(collection_new_template(data));

    form_button_state[collection] = true;

    emile('new-' + collection + '-form', 'height: 0px', {
      duration: 500,
      after: function(){
        var px_regex = /px$/;

        var collection_title_height = parseInt($('#new-' + collection + '-body .boxes-title').css('height').replace(px_regex, ''));
        var collection_summary_height = parseInt($('#new-' + collection + '-body .boxes-summary').css('height').replace(px_regex, ''));
        var collection_total_height = collection_title_height + collection_summary_height + 41;

        emile('new-' + collection + '-body', 'height: ' + collection_total_height + 'px', {
          duration: 500,
          after: function(){
            var collection_id = collection + '-' + data._id
            var collection_body_template = doT.template($('#' + collection + '-body-template').html());

            $('#new-' + collection + '-body').css('height', 'auto').attr('style', null);
            $('#new-' + collection + '-body').attr('id', collection_id);
            $('#new-' + collection + '-form').remove();

            $('#' + collection_id).before(collection_body_template());

            $('#new-' + collection + '-button, #cancel-' + collection + '-button').off('click');
            $('#new-' + collection + '-button, #cancel-' + collection + '-button').on('click', function(e){
              $(document.body).trigger('show:form', [collection]);
            });

            emile('new-' + collection + '-body', 'margin-top: 3px', {duration: 500});
          }
        });
      }
    });
  });

  $(document).on('post:validation', function(e){
    $('#post-form').isHappy({
      fields: {
        '#post-form-body-textarea': {
          required: true,
          message: "Sorry can't submit it if there is no body"
        }
      }
    });
  });

  $(document).on('sign:form', function(e){
    $(document.body).trigger('form:warning');

    if (form_sign_state) {
      $('#sign-form-container').removeClass('hidden');
      emile('sign-form-container', 'opacity: 1;', {
        duration: 300,
        after: function(){
          form_sign_state = false;
          $('#user-sign-in-form-username-input').focus();
        }
      });
    } else {
      emile('sign-form-container', 'opacity: 0;', {
        duration: 300,
        after: function(){
          form_sign_state = true;
          $('#user-sign-in-form, #user-sign-up-form').children(['input', 'textarea']).val('');
          $('#user-sign-in-form, #user-sign-up-form').children(['input', 'textarea']).removeClass('unhappy');
          $('#user-sign-in-form, #user-sign-up-form').children(['.unhappyMessage']).remove();

          $('#sign-form-container').addClass('hidden');
        }
      });
    }
  });

  $(document).on('user:signin', function(e, data){
    form_sign_state = false;
    $(document.body).trigger('sign:form');

    var current_username = data.username;

    $('#sign-up-button').off('click');
    $('#sign-up-button').attr('id', 'sign-out-button');
    $('#sign-out-button .action-text').html(current_username + ' SIGN-OUT');

    if (current_location[1] == '' && data.administrator) {
      var category_body_template = doT.template($('#category-body-template').html());

      $('#first-container').prepend(category_body_template());

      emile('new-category-body', 'margin-top: 3px', {duration: 500, after: function(){
        $('#new-category-button, #cancel-category-button').off('click');
        $('#new-category-button, #cancel-category-button').on('click', function(e){
          $(document.body).trigger('show:form', ['category']);
        });
      }});
    } else if (current_location[3] == 'topics') {
      var collection_ids = {category: {_id: current_location[2]}, topic: {_id: current_location[4]}};
      var post_form_container_template = doT.template($('#post-form-container-template').html());

      $('#third-container').prepend(post_form_container_template(collection_ids));

      $(document.body).trigger('form:warning');
      $(document.body).trigger('post:validation');

      emile('post-form-container', 'margin-top: 0px', {duration: 500});
    } else if (current_location[1] == 'categories' ) {
      var category_id = {category: {_id: current_location[2]} };
      var topic_body_template = doT.template($('#topic-body-template').html());

      $('#second-container').prepend(topic_body_template(category_id));

      emile('new-topic-body', 'margin-top: 3px', {duration: 500, after: function(){
        $('#new-topic-button, #cancel-topic-button').off('click');
        $('#new-topic-button, #cancel-topic-button').on('click', function(e){
          $(document.body).trigger('show:form', ['topic']);
        });
      }});
    }

    $('#sign-out-button').off('click');
    $('#sign-out-button').on('click', function(e){
      $(document.body).trigger('user:signout');
    });
  });

  $(document).on('user:signout', function(e, data){
    $(document.body).trigger('form:send');
    $.ajax({
      type: 'DELETE',
      url: '/users/signout',
      success: function(data){
        var category_body = $('#new-category-body').html();
        var topic_body = $('#new-topic-body').html();
        var post_form_container = $('#post-form-container').html();

        $('#sign-out-button').attr('id', 'sign-up-button');
        $('#sign-up-button .action-text').html('SIGN-IN / SIGN-UP');

        $(document.body).trigger('form:complete');

        if (category_body) {
          emile('new-category-body', 'margin-top: -28px', {duration: 500, after: function(){
            $('#new-category-body').remove();
          }});
        }

        if (topic_body) {
          emile('new-topic-body', 'margin-top: -28px', {duration: 500, after: function(){
            $('#new-topic-body').remove();
          }});
        }

        if (post_form_container) {
          emile('post-form-container', 'margin-top: -140px', {duration: 500, after: function(){
            $('#post-form-container').remove();
          }});
        }

        $('#sign-up-button').off('click');
        $('#sign-up-button').on('click', function(e){
          $(document.body).trigger('sign:form');
        });

      }
    });
  });

  // }}
  
})
