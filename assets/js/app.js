//= require lib/zepto.min
//= require lib/happy.min
//= require lib/moment.min
//= require lib/davis.min
//= require lib/emile.min
//= require lib/doT.min
//= require lib/spin.min
//= require helper
//= require routes
//= require events

Zepto(function($){

  if (current_location[3] == 'topics') {
    topicEventSource(current_location[2], current_location[4]);

    $(document.body).trigger('post:validation');
  }

  $('.relative-date').each(function(index, item){
    var current_time = $(item).html();

    $(item).html(moment.unix(current_time).fromNow());
  });

  var spin_el = document.getElementById('loading-indicator');
  var spinner = new Spinner(spin_opts).spin(spin_el);

  $('body').on('click', function(e){
    if (!form_sign_state) {
      $(document.body).trigger('sign:form');
    }
  });

  $('#sign-form-container').on('click', function(e){
    form_sign_state = true;
  });

  $('#sign-form-container').on('mouseleave', function(e){
    form_sign_state = false;
  });

  $('#user-sign-in-form').isHappy({
    fields: {
      '#user-sign-in-form-username-input': {
        required: true,
        message: "Sorry, username can't be blank"
      },

      '#user-sign-in-form-password-input': {
        required: true,
        message: "Surely the password can't be empty"
      }
    }
  });

  $('#user-sign-up-form').isHappy({
    fields: {
      '#user-sign-up-form-username-input': {
        required: true,
        message: "Sorry, username can't be that short",
        test: formHelper.minLength,
        arg: 6
      },

      '#user-sign-up-form-password-input': {
        required: true,
        message: "The password needs to be longer than that",
        test: formHelper.minLength,
        arg: 6
      },

      '#user-sign-up-form-password-confirmation-input': {
        required: true,
        message: "Your password doesn't match up",
        test: formHelper.equal,
        arg: function(){
          return $('#user-sign-up-form-password-input').val();
        }
      }
    }
  });

  $(document.body).trigger('form:warning');

  $('#new-topic-button, #cancel-topic-button').on('click', function(e){
    $(document.body).trigger('show:form', ['topic']);
  });

  $('#new-category-button, #cancel-category-button').on('click', function(e){
    $(document.body).trigger('show:form', ['category']);
  });

  $('#sign-up-button').on('click', function(e){
    $(document.body).trigger('sign:form');
  });

  $('#sign-out-button').on('click', function(e){
    $(document.body).trigger('user:signout');
  });

})
