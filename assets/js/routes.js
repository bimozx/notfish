Zepto(function($){
  
  // Davis/pushState section {{

  Davis.zepto();

  var app = Davis(function(){
    this.before(function(req){
      var topic_data_id = $('#topic-data-id').val();

      if (req.method === 'get') {
        current_location = req.fullPath.split('/');
      }

      if (typeof topic_data_id != 'undefined' && current_location[3] != 'topics'){
        topic_eventsource_connections[topic_data_id].close();
      }

    });

    this.get('/', function(req){
      var topic_index_container = $('#first-container').html();

      if (topic_index_container == '\n') {
        $(document.body).trigger('form:send');

        $.getJSON('/', function(data){ 
          var category_index_template = doT.template($('#category-index-template').html());
          $('#first-container').html(category_index_template(data));

          var first_container_height = $('#first-container').css('height');
          $('#slide-container').css('margin-left', '-960px');

          $('#new-category-button, #cancel-category-button').on('click', function(e){
            $(document.body).trigger('show:form', ['category']);
          });

          emile('slide-container', 'margin-left: 0;', {duration: 500, after: function(){
              $(document.body).trigger('form:complete');
            }
          });
        });
      } else {
        emile('slide-container', 'margin-left: 0;', {duration: 500});
      }
    });

    this.post('/categories/', function(req){
      formPostHelpers('#category-form', function(data){
        $(document.body).trigger('create:collection', ['category', data]);

        req.redirect('/');
      });
    });

    this.get('/categories/:id', function(req){
      var category_id = req.params['id'];
      var category_index_container = $('#second-container').html();

      $(document.body).trigger('form:send');

      $.getJSON('/categories/' + category_id, function(data){ 
        var category_show_template = doT.template($('#category-show-template').html());
        $('#second-container').html(category_show_template(data));

        $(document.body).trigger('form:complete');

        emile('slide-container', 'margin-left: -960px;', {duration: 500, after: function(){
            //var current_eventsource = eventsource_connections[req.params['id']];
            //if (!$.isObject(current_eventsource) || current_eventsource.readyState == 2){
              //topicEventSource(req.params['id']);
            //}

            $('#new-topic-button, #cancel-topic-button').off('click');
            $('#new-topic-button, #cancel-topic-button').on('click', function(e){
              $(document.body).trigger('show:form', ['topic']);
            });

            $(document.body).trigger('form:complete');
            $(document.body).trigger('form:warning');

          }
        });
      });
    });

    this.get('/categories/:category_id/topics/:id', function(req){

      var topic_id = req.params['id'];
      var category_id = req.params['category_id'];
      var topic_index_container = $('#second-container').html();

      if (topic_index_container !== '\n') {
        $(document.body).trigger('form:send');

        $.getJSON('/categories/' + category_id + '/topics/' + topic_id, function(data){ 
          var topic_show_template = doT.template($('#topic-show-template').html());
          $('#third-container').html(topic_show_template(data));

          $(document.body).trigger('form:complete');

          emile('slide-container', 'margin-left: -1920px;', {duration: 500, after: function(){
              var current_eventsource = topic_eventsource_connections[req.params['id']];
              if (!$.isObject(current_eventsource) || current_eventsource.readyState == 2){
                topicEventSource(req.params['category_id'], req.params['id']);
              }

              $(document.body).trigger('form:warning');
              $(document.body).trigger('form:complete');

              $(document.body).trigger('post:validation');

            }
          });
        });
      }
    });
;
    this.post('/categories/:category_id/topics/', function(req){
      formPostHelpers('#topic-form', function(data){
        $(document.body).trigger('create:collection', ['topic', data]);

        req.redirect('/categories/' + req.params['category_id'] + '/topics/');
      });
    });

    this.post('/categories/:category_id/topics/:id/posts', function(req){
      formPostHelpers('#post-form', function(){

        req.redirect('/categories/' + req.params['category_id'] + '/topics/' + req.params['id']);
      });
    });

    this.post('/users/', function(req){
      formPostHelpers('#user-sign-up-form', function(data){

        if (data.error) {
          alert(data.errors[0]);
        } else {
          $(document.body).trigger('user:signin', [data]);
        }
      });
    });

    this.post('/users/signin', function(req){
      formPostHelpers('#user-sign-in-form', function(data){

        if (data.error) {
          alert("username or password doesn't match, please try again");
        } else {
          $(document.body).trigger('user:signin', [data]);
        }
      });
    });
  });

  app.start();

  //}}
})
