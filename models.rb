class User

  USER_ALLOWED_ATTRIBUTES = ['username', 'password']

  include Mongoid::Document
  include Mongoid::Timestamps

  include BCrypt

  has_many :topics, class_name: 'Topic', inverse_of: :created_by
  has_many :posts, class_name: 'Post', inverse_of: :author

  field :_id, type: String, default: ->{username}
  field :username, type: String 
  field :password_hash, type: String 
  field :administrator, type: Boolean, default: false

  validates :username, uniqueness: true, length: { minimum: 6 }
  validates :username, :password_hash, presence: true
  validates :password_hash, confirmation: true

  def self.signup(fields)
    current_user = User.new(username: fields['username'])
    current_user.password = fields['password']
    current_user.save

    return current_user.errors.full_messages
  end

  def password
    @password ||= Password.new(password_hash)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_hash = @password
  end
end

class Category

  CATEGORY_ALLOWED_ATTRIBUTES = ['title', 'description']

  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :topics

  field :_id, type: String, default: ->{title}
  field :title, type: String
  field :description, type: String

  validates :title, :description, presence: true
  validates :title, uniqueness: true
end

class Topic

  TOPIC_ALLOWED_ATTRIBUTES = ['title', 'body']

  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :category
  belongs_to :created_by, class_name: 'User'
  has_many :posts

  field :title, type: String
  field :body, type: String

  validates :title, :body, presence: true
end

class Post

  POST_ALLOWED_ATTRIBUTES = ['body']

  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :author, class_name: 'User'
  belongs_to :topic

  field :body, type: String

  validates :body, presence: true
end
