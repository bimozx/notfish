post '/categories/:category_id/topics/:id/posts/?' do
  response.headers['Pragma'] = 'no-cache'
  response.headers['Cache-Control'] = 'no-cache'

  parameters = params[:post].keep_if do |key, value|
    Post::POST_ALLOWED_ATTRIBUTES.include? key
  end

  current_topic = Topic.find(params[:id])
  channel_name = 'topic-' + params[:id]

  if current_topic && @current_user
    post_data = Post.new(body: parameters['body'], author: @current_user)
    post = current_topic.posts.push(post_data)
    current_post = post.last

    message_data = "data: #{ encode_to_json({post: post_data}) }\n\n"

    redis = Redis.new

    redis.publish(channel_name, message_data)
    redis.quit

    if request.xhr?
      json({message: 'Your post is posted'})
    else
      redirect to('/');
    end
  else
    404
  end
end

