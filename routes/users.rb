post '/users/' do
  parameters = params[:user].keep_if do |key, value|
    User::USER_ALLOWED_ATTRIBUTES.include? key
  end

  current_user = User.signup(parameters)
  signup_content = current_user.empty? ? {username: parameters[:username], message: 'Login is successful', error: false} : {error: true, errors: current_user}

  if request.xhr?
    json(signup_content)
  else
    redirect to('/')
  end
end

post '/users/signin' do
  parameters = params[:user].keep_if do |key, value|
    User::USER_ALLOWED_ATTRIBUTES.include? key
  end

  current_user = User.find(parameters['username'])
  signin_content = current_user && current_user.password == parameters['password'] ?
                   {username: current_user.username, administrator: current_user.administrator, message: 'Login is successful', error: false} :
                   {message: 'Username or Password is incorrect', error: true}

  session[:user_id] = current_user.id unless signin_content[:error]

  if request.xhr?
    json(signin_content)
  else
    redirect to('/')
  end
end

delete '/users/signout' do
  session[:user_id] = nil unless session[:user_id].nil?

  if request.xhr?
    return encode_to_json({message: 'Logout is successful'})
  else
    redirect to('/')
  end
end

