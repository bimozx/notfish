get '/' do
  @categories = Category.desc(:created_at)
  logged_in = not(@current_user.nil?)

  if request.xhr?
    category_index_json = {user: logged_in, categories: @categories}
    category_index_json[:administrator] = @current_user.administrator if logged_in

    json category_index_json
  else
    erb :'/category/index'
  end
end

post '/categories/?' do
  response.headers['Pragma'] = 'no-cache'
  response.headers['Cache-Control'] = 'no-cache'

  parameters = params[:category].keep_if do |key, value|
    Category::CATEGORY_ALLOWED_ATTRIBUTES.include? key
  end

  if @current_user && @current_user.administrator
    category = Category.new(title: parameters['title'], description: parameters['description'])
    category.save
    category_content = category.errors.empty? ? category : {errors: category.errors.full_messages}

    if request.xhr?
      status 412 if category_content[:errors]
      json(category_content)
    else
      redirect to('/')
    end
  else
    if request.xhr?
      412
    else
      redirect to('/')
    end
  end
end

get '/categories/:id/?' do
  @category = Category.find(params[:id])

  unless @category.nil?
    @topics = @category.topics.desc(:id).limit(9)

    if request.xhr?
      logged_in = not(@current_user.nil?)
      admin = logged_in && @current_user.administrator

      category_show_json = {user: logged_in, administrator: admin, category: @category, topics: @topics}
      json category_show_json
    else
      erb :'/category/show'
    end
  else
    404
  end
end

