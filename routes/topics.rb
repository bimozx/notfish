post '/categories/:id/topics/?' do
  response.headers['Pragma'] = 'no-cache'
  response.headers['Cache-Control'] = 'no-cache'

  parameters = params[:topic].keep_if do |key, value|
    Topic::TOPIC_ALLOWED_ATTRIBUTES.include? key
  end

  @category = Category.find(params[:id])

  if @current_user && @category.nil?
    topic = Topic.new(title: parameters['title'], body: parameters['body'], created_by: @current_user, category: @category)
    topic.save
    topic_content = topic.errors.empty? ?  topic : {errors: topic.errors.full_messages}

    if request.xhr?
      json(topic_content)
    else
      redirect to('/')
    end
  else
    if request.xhr?
      status 412
      json({error: 'whoops'})
    else
      flash[:error] = 'Whoops, something went wrong, please try again...'
      redirect to('/')
    end
  end
end

get '/categories/:category_id/topics/:id/?' do
  @category = Category.find(params[:category_id])
  @topic = @category && @category.topics.find(params[:id])

  unless @topic.nil?
    if request.xhr?
      logged_in = not(@current_user.nil?)
      topic_show_json = {user: logged_in, topic: @topic, category: @category, posts: @topic.posts}
      json topic_show_json
    else
      @posts = @topic.posts
      erb :'/topic/show'
    end
  else
    404
  end
end

