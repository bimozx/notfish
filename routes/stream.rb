get '/categories/:category_id/topics/:id/events', provides: 'text/event-stream' do
  response.headers['X-Accel-Buffering'] = 'no'

  channel_name = 'topic-' + params[:id]
  stream :keep_open do |out|
    redis = EM::Hiredis.connect

    out.errback do
      logger.warn "lost connection"
      redis.unsubscribe(channel_name) if out.closed?
      out.flush
    end

    out.callback do
      redis.unsubscribe(channel_name) if out.closed?
      out.flush
    end

    redis.subscribe(channel_name)
    redis.unsubscribe(channel_name) if out.closed?

    redis.on(:message) do |channel, message|
      out << message unless out.closed?
      redis.unsubscribe(channel_name) if out.closed?
      out.close
    end
  end
end
