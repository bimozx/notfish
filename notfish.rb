## Libraries or Dependencies

[ 'sinatra',
  'sinatra/contrib', 
  'sinatra/synchrony', 
  'sinatra/streaming', 
  'rack-flash', 
  'redis', 
  'em-hiredis', 
  'yaml', 
  'yajl', 
  'json', 
  'bcrypt' ].each do |r|
  require r
end

# Configuration

if development?
  require 'pry'
  require 'pry-nav'
  require 'sinatra/reloader'
end

configure do
  use Rack::Mongoid::Middleware::IdentityMap
  use Rack::Session::Pool, expire_after: 2592000
  use Rack::Flash

  ENV['RACK_ENV'] ||= 'development'

  set :threaded, true

  set :json_encoder, Yajl::Encoder

  Mongoid.raise_not_found_error = false
end

configure :development do
  enable :logging

  Mongoid.logger = Logger.new($stdout)
  Moped.logger = Logger.new($stdout)
end

## Helpers

helpers do
  include ERB::Util

  def encode_to_json(parameters)
    Yajl::Encoder.encode(parameters)
  end
end

## Models and DB Configuration

Mongoid.load!("./config/mongoid.yml")

require './models'

## Routes

Dir['./routes/*.rb'].each do |f|
  require f
end
